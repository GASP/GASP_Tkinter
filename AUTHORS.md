# GASP Development Team

## Principal Developers:

* Richard Crook and Gareth McCaughan (as creators of LiveWires beginners.py)
* Jeff Elkner

## Significant Contributors:

* Nabil Rahman
* Caleb Moran
* Richard Martinez