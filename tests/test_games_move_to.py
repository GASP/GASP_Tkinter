from gasp import games
from gasp import color


messages = ["Here is a circle", "Moving it right", "Moving it to the left", "Stay still...", "Don't Move!!", "Done!"]


class MyScreen(games.Screen):
    item = 0

    def keypress(self, key):
        if key == games.K_SPACE:
            self.item += 1

    def tick(self):
        global text, first_time

        if first_time:
            first_time = 0
            return

        if self.item == 1:
            text.set_text(messages[1])
            circle.move_to(500, 240)

        elif self.item == 2:
            text.set_text(messages[2])
            circle.move_to(200, 240)

        elif self.item == 3:
            text.set_text(messages[3])
            circle.move_to(200, 240)

        elif self.item == 4:
            text.set_text(messages[4])
            circle.move_to(200, 240)

        elif self.item == 5:
            text.set_text(messages[5])

        elif self.item == 6:
            self.quit()


screen = MyScreen()
screen.set_background_color(color.LIGHTGRAY)

text = games.Text(screen, 320, 35, messages[0], size=35, color=color.BLACK)
circle = games.Circle(screen, 320, 240, radius=50, color=color.RED)

first_time = 1
screen.mainloop()
