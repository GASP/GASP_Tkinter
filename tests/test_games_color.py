from gasp import games
from gasp import color


class ColorDemo(games.Screen):

    def __init__(self):
        self.init_screen(width=600, height=600, title="Games Color Demo")

        self.color_names = color.available(names_only=True)
        self.index = 0

        self.text = games.Text(self, 300, 300, "", size=20, color=color.BLACK)
        controls = "Space to move forward; Backspace to move backward; Escape to quit"
        self.controls = games.Text(self, 300, 590, controls, size=15, color=color.BLACK)

        self.last_key = 0
        self.held_for = 0

        self.update()

    def tick(self):
        # Allow for holding keys
        if self.is_pressed(self.last_key):
            self.held_for += 1
        else:
            self.held_for = 0

        if self.held_for >= 10:
            if self.is_pressed(self.last_key):
                self.keypress(self.last_key)

    def keypress(self, key):
        self.last_key = key

        if key == games.K_SPACE:
            self.index = (self.index + 1) % len(self.color_names)
        elif key == games.K_BACKSPACE:
            self.index = (self.index - 1) % len(self.color_names)
        elif key == games.K_ESCAPE:
            self.quit()

        self.update()

    def update(self):
        self.name = self.color_names[self.index]
        self.rgb = color.from_str(self.name)
        self.set_background_color(self.rgb)

        text_color = color.BLACK if self.name != "BLACK" else color.WHITE
        self.text.set_text(f"{self.name}: {self.rgb}")
        self.text.set_color(text_color)
        self.controls.set_color(text_color)


screen = ColorDemo()
screen.mainloop(fps=10)
