from gasp import games
from gasp import color
import string

UPPERS = string.ascii_uppercase
LOWERS = string.ascii_lowercase
DIGITS = string.digits
PUNCTUATION = string.punctuation
DY = 100

TEXT_COLOR = color.RED
TEXT_SIZE = 30


class FontDemo(games.Screen):
    def __init__(self):
        self.init_screen(width=700, height=600, title="Text Font Demo")

        self.fonts = sorted(games.Text.available_fonts())  # Alphabetical Order
        self.index = 0

        x, y = self.center_pos()
        self.uppers = games.Text(self, x, DY, UPPERS, size=TEXT_SIZE, color=TEXT_COLOR)
        self.lowers = games.Text(self, x, 2*DY, LOWERS, size=TEXT_SIZE, color=TEXT_COLOR)
        self.digits = games.Text(self, x, 3*DY, DIGITS, size=TEXT_SIZE, color=TEXT_COLOR)
        self.punctuation = games.Text(self, x, 4*DY, PUNCTUATION, size=TEXT_SIZE, color=TEXT_COLOR)
        self.display = games.Text(self, x, 5*DY, "None", size=TEXT_SIZE, color=color.WHITE)
        games.Text(self, x, 5*DY + 50, f"Available Fonts: {len(self.fonts)}", size=TEXT_SIZE, color=color.WHITE)

        self.update()

    def keypress(self, key):
        if key == games.K_SPACE:
            self.index = (self.index + 1) % len(self.fonts)
        elif key == games.K_BACKSPACE:
            self.index = (self.index - 1) % len(self.fonts)
        elif key == games.K_ESCAPE:
            self.quit()
        self.update()

    def update(self):
        font = self.fonts[self.index]
        for text in [self.uppers, self.lowers, self.digits, self.punctuation]:
            text.set_font(font)
        self.display.set_text(font)


fontdemo = FontDemo()
fontdemo.mainloop()
